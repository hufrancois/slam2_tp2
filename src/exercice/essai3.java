
package exercice;

import donnees.Dao;
import entites.Club;
import java.util.Scanner;


public class essai3 {
    
    public static void main(String[] args) {
        
        Scanner clavier= new Scanner(System.in); 
        String codeClubSaisi;
        String catSaisi;
        
        System.out.print("Entrer le code du club : ");
        codeClubSaisi=clavier.next();
        System.out.println("Entrer la catégorie de poids :");
        catSaisi=clavier.next();
        System.out.println();
        
        Club club = Dao.getClubDeCode(codeClubSaisi);
        
        System.out.println("Nom du Club     : "+club.getNomClub());
        System.out.println("Adresse du Club : "+club.getAdrClub());
        
  
    }
    
}
