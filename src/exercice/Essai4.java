package exercice;

import donnees.Dao;
import entites.Judoka;

import java.util.Scanner;
import static utilitaires.UtilDate.ageEnAnnees;
import static utilitaires.UtilTriListe.trierLesJudokasParNomPrenom;

public class Essai4 {
    
    public static void main(String[] args) {
        
        
        Scanner clavier= new Scanner(System.in); 
        String categorie;
        
        System.out.print("Entrer la catégorie des Judokas à afficher: ");
        categorie=clavier.next();
        System.out.println();
        
        trierLesJudokasParNomPrenom(Dao.tousLesJudokasDeCategorie(categorie));
        
        for(Judoka j : Dao.tousLesJudokasDeCategorie(categorie)){
            System.out.println();
            System.out.printf("%-20s %10s %10s %2d ans %3d kg %3d victoires",j.getLeClub().getNomClub(),j.getNom(),j.getPrenom(),ageEnAnnees(j.getDateNaiss()),j.getPoids(),j.getNbVictoires());
        }
        System.out.println("\n");
    }
}
