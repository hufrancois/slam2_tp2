
package exercice;

import donnees.Dao;
import entites.Club;
import java.util.Scanner;

public class essai2 {
    
  
    public static void main(String[] args) {
        
        Scanner clavier= new Scanner(System.in); 
        String codeClubSaisi; 
        
        System.out.print("Entrer le code du club à afficher: ");
        codeClubSaisi=clavier.next();
        System.out.println();
        
        Club club= Dao.getClubDeCode(codeClubSaisi);
        
        System.out.println("Nom du Club     : "+club.getNomClub());
        System.out.println("Adresse du Club : "+club.getAdrClub());
        System.out.println("Nombre de judokas : "+club.nbJudokas());  
        System.out.println("Nombre de judokas féminin : "+club.nbJudokas("F"));
        System.out.println("Nombre de judokas masculin : "+club.nbJudokas("M"));
  
    }
}
    

