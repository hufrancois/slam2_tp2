package entites;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import static utilitaires.UtilDate.ageEnAnnees;
import static utilitaires.UtilDojo.determineCategorie;

@Entity
public class Judoka implements Serializable  {
   
   @Id
   private Long      id; 
 
   private String    nom;    private String    prenom;
   private String    sexe;
    
   @Temporal(javax.persistence.TemporalType.DATE)
   private Date      dateNaiss;    
   private int       poids;
   private String    ville;
   private int       nbVictoires;
    
   @ManyToOne
   private Club      leClub;
    
   public Judoka() {}

   //<editor-fold defaultstate="collapsed" desc="Méthodes publiques">
   
    public void affichageConsole(){
       
       String format="%4d %-12s %-10s %-2s %3d ans %4d kg %-12s %-15s %3d Victoires";
       System.out.printf(format,
               id,
               nom,
               prenom,
               sexe,
               ageEnAnnees(dateNaiss),
               poids,
               determineCategorie(sexe,poids),
               ville,
               nbVictoires
               );
   }
   
   //</editor-fold>
   //<editor-fold defaultstate="collapsed" desc="Getters et setters">
    
    public Long   getId() { return id;}
    public void   setId(Long id) {this.id = id;}
 
    public String getNom() { return nom;}   
    public void   setNom(String nom) { this.nom = nom;}
    
    public String getPrenom() {
        return prenom;
    }
    public String getSexe() {
        return sexe;
    }
    public void   setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public void   setSexe(String sexe) {
        this.sexe = sexe;
    }
    public Date   getDateNaiss() {
        return dateNaiss;
    }
    public void   setDateNaiss(Date dateNaiss) {
        this.dateNaiss = dateNaiss;
    }
    public int    getPoids() {
        return poids;
    }
    public void   setPoids(int poids) {
        this.poids = poids;
    }
    public String getVille() {
        return ville;
    }
    public void   setVille(String ville) {
        this.ville = ville;
    }
    public int    getNbVictoires() {
        return nbVictoires;
    }
    public void   setNbVictoires(int nbVictoires) {
        this.nbVictoires = nbVictoires;
    }
   
     public Club getLeClub() {
        return leClub;
    }

    public void setLeClub(Club leClub) {
        this.leClub = leClub;
    }
    
    //</editor-fold>
   //<editor-fold defaultstate="collapsed" desc="Redéfinition de la méthode toString">
    
    @Override
    public String toString() {
        
        return nom + " " + prenom + " " + nbVictoires + " victoires";
    }
    //</editor-fold>     
   //<editor-fold defaultstate="collapsed" desc="HashCode & Equals">
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Judoka other = (Judoka) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    //</editor-fold>
}


