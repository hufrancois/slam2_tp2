package exemplesducours;

import donnees.Dao;
import entites.Judoka;
import static utilitaires.UtilDate.aujourdhuiChaine;

public class Exemple2_Aff_TousLesJudokas {

    public static void main( String[ ] args) {

        String decalage="      ";
        
        System.out.print("\n"+ decalage);
        
        String format= " Liste de tous les Judokas ( tous clubs confondus ) à la date du: %-10s \n\n";
       
        System.out.printf(format, aujourdhuiChaine());
     
        for( Judoka judoka: Dao.getTousLesJudokas() ) {
            
            System.out.print(decalage); 
            judoka.affichageConsole();
            System.out.println();
        }
        
        System.out.println();
    }
}


